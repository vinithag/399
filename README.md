To run these notebooks, the following dataset must be downloaded into the repo:

https://www.kaggle.com/aaronschlegel/austin-animal-center-shelter-outcomes-and/data
The file is aac_shelter_outcomes.csv